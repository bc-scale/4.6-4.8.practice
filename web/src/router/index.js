import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from '../views/Login.vue'
import Profile from '../views/Profile.vue'
import store from '../store'
import Friends from '../views/Friends.vue'
import ProfileEditor from '../views/ProfileEditor.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/login',
    name: 'Login',
    component: Login
  },
  {
    path: '/',
    name: 'Main',
    component: Profile,
    props: () => ({ id: store.state.user.profile.id })
  },
  {
    path: '/friends',
    name: 'Friends',
    component: Friends
  },
  {
    path: '/profile/:id',
    name: 'Profile',
    component: Profile,
    props: (route) => ({ id: route.params.id })
  },
  {
    path: '/profile-editor',
    name: 'ProfileEditor',
    component: ProfileEditor
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

router.beforeEach((to, from, next) => {
  if (to.name !== 'Login' && !Vue.prototype.$api.isAuth()) {
    next({ name: 'Login' })
  } else if (to.name === 'Login' && Vue.prototype.$api.isAuth()) {
    next({ name: 'Main' })
  } else {
    next()
  }
})

export default router
