import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    user: null
  },
  getters: {
  },
  mutations: {
    setUser(state, user) {
      state.user = user
    }
  },
  actions: {
    async fetchCurrentUser({ commit }) {
      const user = await Vue.prototype.$api.getCurrentUser()
      commit('setUser', user)
    }
  },
  modules: {
  }
})
