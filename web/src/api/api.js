export class IProvider {
    getClient() {}
    setAuthHeader() {}
    clearAuthHeader() {}
}

export class APIService {
    constructor(provider) {
        this.provider = provider
        this.client = provider.getClient()
        this.token = window.localStorage.getItem('token')
        if (this.token) {
            this.provider.setAuthHeader(this.token)
        }
    }

    completeUrl(url) {
        return this.provider.baseURL + url
    }

    isAuth() {
        return !!this.token
    }

    async login(identifier, password) {
        const {data} = await this.client.post('auth/local', {
            identifier,
            password
        })

        this._setToken(data.jwt)
    }

    logout() {
        this.token = ''
        this.provider.clearAuthHeader()
        window.localStorage.removeItem('token')
    }

    async getCurrentUser() {
        const { data: user } = await this.client.get('users/me')
        const { data: profile } = await this.client.get(`profiles/${user.profile}`)

        user.profile = profile

        return user
    }

    async getProfile(id) {
        const { data } = await this.client.get(`profiles/${id}`)
        return data
    }

    async getProfiles() {
        const { data } = await this.client.get(`profiles`)
        return data
    }

    async saveProfile(profile) {
        await this.client.put(`profiles/${profile.id}`, profile)
    }

    async upload(form) {
        const { data } = await this.client.post('upload', form)
        return data
    }

    _setToken(token) {
        if (!token) {
            return
        }
        this.token = token
        this.provider.setAuthHeader(token)
        window.localStorage.setItem('token', token)
    }
}
